//
//  main.m
//  htc2u_info
//
//  Created by chow hoo tuck on 2016/12/26.
//  Copyright © 2016年 chow hoo tuck. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
