//
//  AppDelegate.h
//  htc2u_info
//
//  Created by chow hoo tuck on 2016/12/26.
//  Copyright © 2016年 chow hoo tuck. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

